# Data Science and Analytics 
## Clemson University

## Sydney Sawyer

### DSA 8640 - Programming for Data Science
1. [Twitter Data: Preliminary Analysis](Preliminary_Analysis/README.md "Twitter Data: Preliminary Analysis")
    - Create a Twitter Developer Account and an app to use the Twitter API
    - Create a Twython Instance with your Developer credentials
    - Collect 1K tweets containing the keyword of your choice
    - Read JSON file for Preliminary Analysis
    - Conduct Preliminary Analysis
    - Create a Word Cloud from your 1K tweets

2. [Twitter Data: Sentiment Analysis](Sentiment_Analysis/README.md "Twitter Data: Sentiment Analysis")
    - Open JSON file and create a list of 1K tweets
    - Create two new lists to hold subjectivity and polarity scores for each tweet
    - Create a histogram of the subjectivity of the tweets
    - Create a histogram of the polarity of the tweets
    - Use topic modeling concepts

### DSA 8010 - Statistical Methods I

### DSA 8030 - Introduction to Statistical Computing
1. [Data Analysis in R](P1_8030/README.md "R Project 1")
    - Import real data set
    - Clean data set as needed
    - Perform analysis on the data to explore questions of interest pertaining to the dataset
    - Create a shiny/flex dashboard that allows a user to explore the data set
    - Summarize findings in report and presentation