# Data Science and Analytics
# DSA 8640 - Programming for Data Science

# Twitter Data: Sentiment Analysis

## Assignment Requirements:
1. Open JSON file and create a list of 1K tweets
2. Create two new lists to hold subjectivity and polarity scores for each tweet
3. Create a histogram of the subjectivity of the tweets
4. Create a histogram of the polarity of the tweets
5. Use topic modeling concepts to:
    - Vectorize the tweets
    - Create a document-term matrix
    - Create a list of unique words
    - Perform NMF decomposition to create 7 topics

## This README.md file should contain the following: 
- Screenshots of Python Code
- Polarity Histograms
- Subjectivity Histogram

## Assignment Screenshots:
*Open JSON file and create two lists (sub_list, pol_list)*:

![Sentiment Analysis](img/sentiment_analysis_1.png)

*Create histograms for subjectivity and polarity scores*:

![Histograms](img/sentiment_analysis_2.png)

*Create a histogram for polarity after removing objective tweets*:

![Polarity 2](img/polarity_3.png)

*Topic Modeling Setup*:

![Topic Modeling](img/topic_modeling_4.png)

*Vectorize the 1K tweets*:

![Vectorize](img/vectorizer_5.png)

*Perform NMF decomposition*:

![NMF Decomposition](img/matrix_6.png)

*Displaying Topics*:

![Keywords](img/keywords_7.png)

*Polarity & Subjectivity Histograms*:

|*Polarity*: |*Subjectivity*: 	|*Polarity New*: 	|
|:-:	|:-:	|:-:	|
| ![Polarity](img/polarity.png) 	|![Subjectivity](img/subjectivity.png) 	|![Polarity New](img/polarity_new.png) 	|

