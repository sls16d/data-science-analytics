# Data Science and Analytics
# DSA 8640 - Programming for Data Science

# Data Analysis in R

## Assignment Requirements:
- Import real data set
- Clean data set as needed
- Perform analysis on the data to explore questions of interest pertaining to the dataset
- Create a shiny/flex dashboard that allows a user to explore the data set
- Summarize findings in report and presentation

## This README.md file should contain the following: 
- Data set file: [CPS1985.csv](CPS1985.csv "Data set link")
- R code file: [P1_ssawye3.Rmd](P1_ssawye3.Rmd)
- Graphs
- R Shiny Dashboard

> CPS1985 is a random sample drawn from a Current Population Survey given by the US Census Bureau in 1985. This sample was originally drawn for Berndt, E.R. (1991) The Practice of Econometrics, Classic and Contemporary. Addison-Wesley Publishing Company, Reading. There are 12 variables, five are numeric, and seven are factor variables. There were 0 missing values in this original data set. One observation in this data represents the hourly wage, years of education, experience, age, ethnicity, region, gender, occupation, sector, union affiliation, and marital status of a working individual in 1985.
> I cleaned this data set by selecting only the columns that I wanted to study in this exploration. I am using four out of  12 variables available, and I have decided to keep the variables the same to save the integrity of the data. 

- gender (factor variable): the gender of the individual selected for the sample (male/female).
    - This data set contains 245 females and 289 males. Below is a table that shows the distribution of genders in this data set.

- education (numeric variable): the number of years of education
    - The mean amount of years of education is 13.01873 years, which is the length of time it takes to graduate high school plus one year. The minimum value is 2 years, and the maximum value is 18 years. 

- wage (numeric variable): the amount per hour that the individual makes at their job. 
      - The mean amount of dollars per hour for the wage variable is 9.024064 dollars. The minimum made per hour is 1 dollar, and the maximum is 44.5 dollars.

- occupation (factor variable): the occupation of the individual. 
    - These factor levels consist of worker(tradesperson or assembly line worker), technical(technical or professional worker), services (service worker), office (office and clerical worker), sales (sales worker), management (management and administration).
    - Here are the amounts of people who work in each of these fields: Management: 55, Office: 97, Sales: 38, Services: 83, Technical: 105, Worker: 156

## Assignment Screenshots:
*Total Male and Female Counts*:

![Gender Totals](img/males_females_1.png)

*Total amount of men and women in each occupation*:

![Gender Occupation Totals](img/men_women_total_occupation_2.png)

*Average Wage for Males and Females*:

![Average Wage](img/men_women_average_wage_3.png)

*Average Wage Per Occupation*
*Male*:
![Male Avg Wage Per Occupation](img/avg_wage_per_occupation_male_5.png)

*Female*: 
![Female Average Wage Per Occupation](img/avg_wage_per_occupation_female_4.png)

*Average Wage Per Occupation Graph*

![Avg Wage Per Occupation Graph](img/gender_jobs_wage_6.png)

*Average Years of Education for Males and Females*:

![Average Total Years of Eduction](img/avg_years_of_education_per_gender_7.png)

*Average Amount of Schooling in Each Occupation*:

![Average Amount of Schooling Per Occupation](img/occupation_education_8.png)

*Average Wage Per Years of Education*:

![Avg Wage Per Years of Education](img/wage_education.png)

