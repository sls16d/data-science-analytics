# Data Science and Analytics
# DSA 8640 - Programming for Data Science

# Twitter Data: Preliminary Analysis

## Assignment Requirements:
1. Create a Twitter Developer Account and an app to use the Twitter API
2. Create a Twython Instance with your Developer credentials
3. Collect 1K tweets containing the keyword of your choice
4. Read JSON file for Preliminary Analysis
5. Conduct Preliminary Analysis
6. Create a Word Cloud from your 1K tweets

## This README.md file should contain the following: 
- Screenshots of Python Code
- Word Cloud image

## Assignment Screenshots:
*Creating a JSON file with my Twitter Developer Credentials*:

![Twitter Developer Credentials](img/json_creds_1.png)

*Collecting 1K tweets containing the keyword*:

|*Part 1*: |*Part 2*: 	|
|:-:	|:-:	|
| ![Collecting 1k Tweets](img/collect_tweets_1_2.png) 	|![Collecting 1k Tweets](img/collect_tweets_2_3.png) 	|

*Read the JSON file*:

![Reading JSON file](img/read_json_4.png)

*Preliminary Analysis*:

![Preliminary Analysis](img/p_analysis_5.png)

*Create Word Cloud*:

|*Part 1*: |*Part 2*: 	|
|:-:	|:-:	|
| ![Create Word Cloud](img/word_cloud_6.png) 	|![Create Word Cloud](img/word_cloud_7.png) 	|

*Word Cloud Output*:

![Word Cloud](img/word_cloud.png)

